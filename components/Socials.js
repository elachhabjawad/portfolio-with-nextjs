
import { RiGitlabFill, RiLinkedinFill, RiCodepenFill, RiInstagramLine, RiTwitterFill,RiFacebookBoxFill, RiYoutubeLine} from 'react-icons/ri'
import Link from 'next/link';


const dataSocials = [
  { path: 'https://about.gitlab.com/', icon: <RiGitlabFill />, title: 'Gitlab' },
  { path: 'https://fr.linkedin.com/', icon: <RiLinkedinFill />, title: 'Youtube' },
  { path: 'https://codepen.io/', icon: <RiCodepenFill />, title: 'Codepen' },
  { path: 'https://www.instagram.com/', icon: <RiInstagramLine />, title: 'Instagram' },
  { path: 'https://twitter.com', icon: <RiTwitterFill />, title: 'X' },
  { path: 'https://www.facebook.com/', icon: <RiFacebookBoxFill />, title: 'Facebook' },
  { path: 'https://www.youtube.com/', icon: <RiYoutubeLine />, title: 'Youtube' },
]

const Socials = () => {
  return (
    <div className="flex items-center gap-x-5 text-lg">

      {dataSocials.map((item, index) => (

        <Link 
          key={index}
          href={item.path}
          title={item.title}
          className={'hover:text-accent transition-all duration-300'}>
          {item.icon}
        </Link>

      ))}

    </div>
  )
}

export default Socials;
