import Image from 'next/image';
import Link from 'next/link';
import { HiArrowRight } from 'react-icons/hi2';

const ProjectsBtn = () => {
  return (
    <div className="mx-auto xl:mx-0">
      <Link
        href={'/work'}
        title={'Work'}
        className={'relative w-[130px] h-[130px] flex justify-center items-center bg-circleStar bg-cover bg-no-repeat group'}
      >
        <Image
          src={'/rounded-text.png'}
          width={95}
          height={95}
          alt={'rounded text'}
          className={'animate-spin-slow w-full max-w-[95px] max-h-[95px]'}
        />
        <HiArrowRight className={'absolute text-2xl group-hover:translate-x-2 transition-all duration-300'} />
      </Link>

    </div>
  );
}

export default ProjectsBtn;
