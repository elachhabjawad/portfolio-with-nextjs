import { BsArrowRight } from 'react-icons/bs'
import Image from 'next/image';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination } from 'swiper';
import 'swiper/css';
import 'swiper/css/free-mode';
import 'swiper/css/pagination';

const workSlides = {
  slides: [
    {
      images: [
        {
          title: 'Project 1',
          path: 'www.project-1.com',
          image: '/thumb1.jpg',
        },
        {
          title: 'Project 2',
          path: 'www.project-2.com',
          image: '/thumb2.jpg',
        },
        {
          title: 'Project 3',
          path: 'www.project-3.com',
          image: '/thumb3.jpg',
        },
        {
          title: 'Project 4',
          path: 'www.project-4.com',
          image: '/thumb4.jpg',
        },
      ],
    },
    {
      images: [
        {
          title: 'Project 5',
          path: 'www.project-5.com',
          image: '/thumb4.jpg',
        },
        {
          title: 'Project 6',
          path: 'www.project-6.com',
          image: '/thumb1.jpg',
        },
        {
          title: 'Project 7',
          path: 'www.project-7.com',
          image: '/thumb2.jpg',
        },
        {
          title: 'Project 8',
          path: 'www.project-8.com',
          image: '/thumb3.jpg',
        },
      ],
    },
  ],
};


const WorkSlider = () => {
  return (
    <Swiper
      spaceBetween={10}
      freeMode={true}
      pagination={{
        clickable: true
      }}
      modules={[Pagination]}
      className="h-[280px] sm:h-[480px] cursor-pointer"
    >

      {workSlides.slides.map((slide, index) => {
        return (
          <SwiperSlide key={index}>
            <div className='grid grid-cols-2 grid-rows-2 gap-4'>
              {slide.images.map((image, index) => {
                return <article key={index}
                  className='relative rounded-lg overflow-hidden 
                flex items-center justify-center group'>
                  <div className='flex items-center justify-center relative overflow-hidden group'>
                    {/* Image */}
                    <Image
                      src={image.image}
                      width={500}
                      height={300}
                      alt={image.title} 
                      />

                    {/* Overlay gradient */}
                    <div className='absolute inset-0 bg-gradient-to-t from-transparent via-[#e838cc] to-[#4a22bd] opacity-0 group-hover:opacity-80 transition-all duration-700'>
                      <div className='absolute bottom-0 left-1/2 translate-y-full -translate-x-1/2 group-hover:-translate-y-10 group-hover:xl:-translate-y-20 transition-all duration-300'>
                        <div className='flex items-center gap-x-2 text-[13px] tracking-[0.2em]'>
                          {/* Title */}
                          <a href={image.path} title={image.title} target="_blank" className="delay-100"><h3>{image.title}</h3></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </article>
              })}
            </div>
          </SwiperSlide>
        )
      })}
    </Swiper>
  )
}

export default WorkSlider;

