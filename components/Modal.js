import { FaCheck, FaTimes } from "react-icons/fa";
import { motion } from 'framer-motion';
import { fadeIn } from '../variants';

const Modal = ({ isOpen, closeModal }) => {
    return (
        <>
            {isOpen && (
                <motion.div
                    variants={fadeIn('down', 0.2)}
                    initial="hidden"
                    animate="show"
                    exit="hidden"
                     className="fixed inset-0 z-10 flex items-center justify-center">
                    <div className="absolute inset-0 bg-black opacity-50" onClick={closeModal}></div>
                    <div className="flex flex-col justify-center items-center w-[330px] h-[250px] xl:w-[500px] xl:h-[300px] gap-y-4 bg-white text-black p-4 rounded-md z-20  ">

                        <div className="flex justify-center items-center w-[80px] h-[80px] xl:h-[100px] xl:w-[100px]  border-4 border-green-700/60 rounded-full">
                            <FaCheck className="text-xl xl:text-3xl text-green-700/80" />
                        </div>

                        <h2 className="text-2xl">Success !</h2>
                        <p className="text-black">Message sent successfully</p>

                        <button className="btn rounded-full  text-white bg-accent max-w-[170px] px-8 transition-all duration-300 flex items-center justify-center overflow-hidden hover:border-accent group" onClick={closeModal}>
                            <span className="group-hover:-translate-y-[120%] group-hover:opacity-0  transition-all duration-500">{"Close"}</span>
                            <FaTimes className="-translate-y-[120%] opacity-0 group-hover:flex group-hover:-translate-y-0 group-hover:opacity-100 transition-all duration-300 absolute text-[22px]" />
                        </button>
                    </div>
                </motion.div>
            )}
        </>
    );
};

export default Modal;
