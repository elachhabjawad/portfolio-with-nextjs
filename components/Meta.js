import Head from 'next/head';

const Meta = ({ title, description }) => (
    <Head>
        <title>{title}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content={description} />
        <meta name="keywords" content="web developer, designer, digital experiences, tech stack, innovation, creativity, user interactions, portfolio, web development, design, front-end, back-end, coding, programming" />
        <meta name="author" content="Jawad EL ACHHAB" />
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
        <link rel="manifest" href="/site.webmanifest" />
        <link rel="canonical" href="https://www.elachhabjawad.me/" />
    </Head>
);

export default Meta;
