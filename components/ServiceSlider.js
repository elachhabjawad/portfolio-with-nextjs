import { RxCrop, RxPencil2, RxDesktop, RxReader, RxRocket, RxArrowRight, RxArrowTopRight } from "react-icons/rx";
import { Swiper, SwiperSlide } from 'swiper/react';
import { FreeMode, Pagination } from 'swiper';
import 'swiper/css';
import 'swiper/css/free-mode';
import 'swiper/css/pagination';


const serviceData = [
  {
    icon: <RxCrop />,
    title: 'Web Solutions',
    description: 'Integrated Services for Cohesive Presence.',
  },
  {
    icon: <RxPencil2 />,
    title: 'SEO Optimization',
    description: 'Tailored Strategies for Top Rankings.',
  },
  {
    icon: <RxDesktop />,
    title: 'Web Development',
    description: 'Seamless Full-Stack Expertise.',
  },
  {
    icon: <RxRocket />,
    title: 'Digital Presence Enhancement',
    description: 'Holistic Approach to Elevate Footprint.',
  },
];




const ServiceSlider = () => {
  return <Swiper
    breakpoints={{
      320: {
        slidesPerView: 1,
        spaceBetween: 15
      },
      640: {
        slidesPerView: 3,
        spaceBetween: 15
      }
    }
    }

    freeMode={true}
    pagination={{
      clickable: true
    }}
    modules={[FreeMode, Pagination]}

    className="h-[240px] sm:h-[340px]">
    {serviceData.map((item, index) => {
      return (
        <SwiperSlide key={index}>
          <article className="bg-[rgba(65,47,123,0.15)] h-max
             rounded-lg px-6 py-8 flex sm:flex-col gap-x-6 
             sm:gap-x-0 group cursor-pointer
              hover:bg-[rgba(89,65,169,0.15)] transition-all duration-300">
            {/* icon */}
            <div className="text-4xl text-accent mb-4"> {item.icon}</div>
            {/* title & dec */}
            <div className="mb-8">
              <h3 className="mb-2 text-lg">{item.title}</h3>
              <p className="max-w-[350px] leading-normal">{item.description}</p>
            </div>
            {/* arrows */}
            <div className="text-3xl">
              <RxArrowTopRight className="group-hover:rotate-45
              group-hover:text-accent transition-all duration-300"/>
            </div>
          </article>
        </SwiperSlide>
      )
    })}
  </Swiper>;
};

export default ServiceSlider;
