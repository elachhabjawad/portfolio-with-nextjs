import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination } from 'swiper';
import { FaQuoteLeft } from 'react-icons/fa'
import Image from 'next/image';

import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';


const testimonialData = [
  {
    image: '/t-avt-1.png',
    name: 'Sophie Williams',
    position: 'UX Designer | Creative Consultant',
    message: 'Jawad is EXCEPTIONAL! If you have any doubt about hiring him, ask me – I am truly impressed by this guy!',
  },
  {
    image: '/t-avt-2.png',
    name: 'Alex Rodriguez',
    position: 'CTO & Co-Founder at Tech Innovators',
    message: 'Jawad is one of the most talented professionals in web development. Among his skills, you can find excellent communication and precision in planning even in complex projects.',
  },
  {
    image: '/t-avt-3.png',
    name: 'Emma Thompson',
    position: 'CEO at Digital Influence Solutions',
    message: 'Since 2018, Jawad has been instrumental in our website development, contributing to our company\'s growth.Despite remote work, he\'s highly responsive, organized, and strategic. Managing daily changes, offering valuable insights, and implementing effective processes, Jawad is a valuable and impactful team member.',
  },
];



const TestimonialSlider = () => {
  return <Swiper

    navigation={true}
    pagination={{
      clickable: true
    }}
    modules={[Navigation, Pagination]}

    className="h-[480px]">
    {testimonialData.map((person, index) => {
      return (
        <SwiperSlide key={index}>
          <article className="flex flex-col items-center md:flex-row gap-x-8 h-full px-16">
            <div className="w-full max-w-[300px] flex flex-col xl:justify-center
            items-center relative mx-auto xl:mx-0">
              <div className="flex flex-col justify-center text-center">
                {/* Avatar */}
                <div className="mb-2 mx-auto">
                  <Image src={person.image} width={100} height={100} alt={person.name} />
                </div>
                {/* Full name */}
                <h3 className="text-lg">{person.name}</h3>
                {/* Position */}
                <h4 className="text-[12px] uppercase font-extralight tracking-widest">
                  {person.position}
                </h4>
              </div>
            </div>
            <div className="flex-1 flex flex-col justify-center  before:w-[1px] xl:before:bg-white/20 xl:before:absolute xl:before:left-0 xl:before:h-[200px] relative xl:pl-20">
              {/* Quote icon */}
              <div className="mb-4">
                <FaQuoteLeft className="text-4xl xl:text-6xl text-white/20 mx-auto md:mx-0" />
              </div>
              {/* Message */}
              <blockquote className="xl:text-lg text-center md:text-left">
                {person.message}
              </blockquote>
            </div>
          </article>
        </SwiperSlide>
      )
    })}
  </Swiper>;
};

export default TestimonialSlider;

