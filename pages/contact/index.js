import { useRef, useState } from 'react';
import { BsArrowRight } from 'react-icons/bs';
import { motion } from 'framer-motion';
import { fadeIn } from '../../variants';
import Meta from '../../components/Meta';
import Modal from '../../components/Modal';



const Contact = () => {

  const nameRef = useRef(null);
  const emailRef = useRef(null);
  const subjectRef = useRef(null);
  const messageRef = useRef(null);
  const [submitted, setSubmitted] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const restForm = () => {
    nameRef.current.value = '';
    emailRef.current.value = '';
    subjectRef.current.value = '';
    messageRef.current.value = '';
  }


  const handelSubmit = (event) => {
    event.preventDefault();

    const name = nameRef.current.value;
    const email = emailRef.current.value;
    const subject = subjectRef.current.value;
    const message = messageRef.current.value;

    setSubmitted(true);
    setIsModalOpen(true);

    if (submitted) {
      restForm();
    }

  }

  const closeModal = () => {
    setIsModalOpen(false);
  };


  const metaData = {
    title: 'Contact - Jawad EL ACHHAB',
    description: 'Get in touch with Jawad EL ACHHAB for collaboration, inquiries, or any questions you may have. Use the contact form or find the available communication channels to connect. Start a conversation and explore opportunities to work together.',
  };

  return (
    <>
      <Meta {...metaData} />

      <div className="h-full bg-primary/30 py-36 flex items-center">

        {submitted ? (
          <Modal isOpen={isModalOpen} closeModal={closeModal} />)
          : ''}

        <div className="container mx-auto">
          <div className="flex flex-col xl:flex-row gap-x-8">
            <section className="text-center flex xl:w-[30vw] flex-col lg:text-left mb-4 xl:mb-0">
              {/* Title */}
              <motion.h2
                variants={fadeIn('up', 0.3)} 
                initial="hidden"
                animate="show"
                exit="hidden"
                className="h2 xl:mt-8"
              >{' Let\'s'} <span className="text-accent">connect</span>
              </motion.h2>
              {/* Infos contact */}
              <motion.div
                variants={fadeIn('up', 0.4)}
                initial="hidden"
                animate="show"
                exit="hidden"
                className="mb-4 max-w-[400px] mx-auto lg:mx-0"
              >
                <ul>
                  <li className="mb-3">
                    <label className="block uppercase text-white/20 mb-2">Address :</label>
                    <p>Street: 41, rue Haj Amar Riffi, City: Rue Haj Amar Riffi, State/Province/Area: Casablanca, Phone Number: 05 22 54 11 54, Zip Code: 20120, Country Calling Code: +212, Country: Morocco</p>
                  </li>
                  <li className="mb-3">
                    <label className="block uppercase text-white/20 mb-2">Email :</label>
                    <a href="mailto:elachhabjawad@gmail.com" className="hover:text-accent duration-300 transition-all">
                      elachhabjawad@gmail.com</a>
                  </li>
                  <li>
                    <label className="block uppercase text-white/20 mb-2">Phone :</label>
                    <a href="tel:+212643664695" className="hover:text-accent duration-300 transition-all">+212643664695</a>
                  </li>

                </ul>
              </motion.div>
            </section>
            <motion.section
              variants={fadeIn('down', 0.4)}
              initial="hidden"
              animate="show"
              exit="hidden"
              className="w-full xl:max-w-[50%]"
            >

              <form
                onSubmit={handelSubmit}
                variants={fadeIn('up', 0.4)}
                initial="hidden"
                animate="show"
                className="flex-1 flex flex-col gap-6 w-full mx-auto">
                {/* input group */}

                <div className="flex gap-x-6 w-full">

                  <input
                    ref={nameRef}
                    name="name"
                    type="text"
                    placeholder="Full name"
                    className="input"
                    required
                  />

                  <input
                    ref={emailRef}
                    type="email"
                    name="email"
                    placeholder="Email Adderss"
                    className="input"
                    pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"
                    required
                  />

                </div>

                <input
                  ref={subjectRef}
                  type="text"
                  name="subject"
                  placeholder="Subject"
                  className="input"
                  required
                />
                <textarea
                  ref={messageRef}
                  name="message"
                  placeholder="Message"
                  className="textarea"
                  required
                ></textarea>

                <button type="submit" className="btn rounded-full border border-white/50 max-w-[170px] px-8 transition-all duration-300 flex items-center justify-center overflow-hidden hover:border-accent group">
                  <span className="group-hover:-translate-y-[120%] group-hover:opacity-0  transition-all duration-500">{'Let\'s talk'}</span>
                  <BsArrowRight className="-translate-y-[120%] opacity-0 group-hover:flex group-hover:-translate-y-0 group-hover:opacity-100 transition-all duration-300 absolute text-[22px]" />
                </button>
              </form>

            </motion.section>
          </div>
        </div>

      </div>


    </>
  )
    ;
};

export default Contact;
