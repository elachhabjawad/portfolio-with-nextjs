
import WorkSlider from '../../components/WorkSlider';
import Bulb from '../../components/Bulb';
import { motion } from 'framer-motion';
import { fadeIn } from '../../variants';
import Meta from '../../components/Meta';

const Work = () => {


  const metaData = {
    title: 'Work & Projects - Jawad EL ACHHAB',
    description: 'Explore Jawad EL ACHHAB\'s diverse portfolio showcasing web development, design, and creative projects. Dive into a collection of work that reflects innovation, attention to detail, and a passion for creating impactful digital experiences. Discover the skills and expertise brought to each project.',
  };


  return (
    <>
      <Meta {...metaData} />
      <div className="h-full bg-primary/30 py-36 flex items-center">
        <div className="container mx-auto">
          <div className="flex flex-col xl:flex-row gap-x-8">
            <section className="text-center flex xl:w-[30vw] flex-col lg:text-left mb-4 xl:mb-0">
              {/* Title */}
              <motion.h2
                variants={fadeIn('up', 0.3)}
                initial="hidden"
                animate="show"
                exit="hidden"
                className="h2 xl:mt-8"
              > My work <span className="text-accent">.</span>
              </motion.h2>
              {/* Body */}
              <motion.p
                variants={fadeIn('up', 0.4)}
                initial="hidden"
                animate="show"
                exit="hidden"
                className="max-w-[400px] mx-auto lg:mx-0"
              >
                {'Greetings! I\'m a Full Stack Web Developer, adept at creating responsive and intuitive web applications.'}
              </motion.p>
              <motion.p
                variants={fadeIn('up', 0.4)}
                initial="hidden"
                animate="show"
                exit="hidden"
                className="mb-4 max-w-[400px] mx-auto lg:mx-0"
              >
                {'My portfolio showcases a diverse range of projects, highlighting my skills in both front-end and back-end development. Explore the intersection of design and functionality in my work'}
              </motion.p>
            </section>
            <motion.section
              variants={fadeIn('down', 0.6)}
              initial="hidden"
              animate="show"
              exit="hidden"
              className="w-full xl:max-w-[65%]"
            >
              {/* Slider */}
              <WorkSlider />
            </motion.section>
          </div>
        </div>
        <Bulb />
      </div>


    </>

  )

    ;
};

export default Work;
