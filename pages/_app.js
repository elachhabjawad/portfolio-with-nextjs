import '../styles/globals.css';

// components
import Layout from '../components/Layout';
import Transition from '../components/Transition';

// router
import { useRouter } from 'next/router';
// frame motion
import { AnimatePresence, motion } from 'framer-motion';



function MyApp({ Component, pageProps }) {

  const router = useRouter();

  return <Layout>
    <AnimatePresence mode='wait'>
      <motion.main key={router.route} className='h-full'>
        <Transition />
        <Component {...pageProps} />
      </motion.main>
    </AnimatePresence>
  </Layout>;
}

export default MyApp;
