import { useState } from 'react';
import { FaHtml5, FaCss3, FaSass, FaBootstrap, FaJs, FaReact, FaVuejs, FaPhp, FaLaravel, FaSymfony, FaWordpress, FaDrupal, FaJira, FaGulp, FaPooStorm } from "react-icons/fa";
import { SiNextdotjs, SiAdobexd, SiAdobephotoshop, SiMysql, SiRedmine } from "react-icons/si";
import { motion } from 'framer-motion';
import { fadeIn } from '../../variants';
import CountUp from 'react-countup';
import Avatar from '../../components/Avatar';
import Circles from '../../components/Circles';
import Meta from '../../components/Meta';

const aboutData = [
  {
    title: 'skills',
    info: [
      {
        title: 'Frontend Development',
        icons: [
          <FaHtml5 key={1} />,
          <FaCss3 key={2} />,
          <FaSass key={3} />,
          <FaBootstrap key={4} />,
          <FaJs key={5} />,
          <FaReact key={6} />,
          <SiNextdotjs key={7} />,
          <FaVuejs key={8} />,

        ],
      },
      {
        title: 'Backend Development',
        icons: [
          <FaPhp key={1} />,
          <SiMysql key={2} />,
          <FaLaravel key={2} />,
          <FaSymfony key={3} />
        ],
      },
      {
        title: 'Content Management System',
        icons: [
          <FaWordpress key={1} />,
          <FaDrupal key={2} />
        ],
      },

      {
        title: 'Tools',
        icons: [
          <SiRedmine key={1} />,
          <FaGulp key={2} />,
          <SiAdobexd key={3} />,
          <SiAdobephotoshop key={4} />,
        ],
      }
    ],
  },
  {
    title: 'Education',
    info: [
      {
        title: 'Database Engineering',
        year: '2020 - 2021',
      },
      {
        title: 'Designer and developer of Java JEE applications',
        year: '2017 - 2018',
      },
      {
        title: 'IT development',
        year: '2015 - 2017',
      },
    ],
  },
  {
    title: 'Experience',
    info: [
      {
        title: 'Full-Stack Web Developer - FORNET',
        year: '2022 - Present ',
      },
      {
        title: 'Full-Stack Web Developer - ULTEOS',
        year: '2011 - 2021',
      },
      {
        title: 'Web Developer PHP - ASIC',
        year: '2019 - 2020',
      },
    ],
  },
  {
    title: 'Certifications',
    info: [
      {
        title: 'Front End Development Libraries - freeCodeCamp',
        year: '2023',
      },
      {
        title: 'JavaScript Algorithms and Data Structures - freeCodeCamp',
        year: '2023',
      },
      {
        title: 'Discover the PHP Laravel framework - OpenClassrooms',
        year: '2020',
      },
      {
        title: 'Develop websites with Java EE - OpenClassrooms',
        year: '2020',
      },
      {
        title: 'PHP Tutorial Course - SoloLearn',
        year: '2019',
      },
      {

        title: 'SQL Fundamentals Course - SoloLearn',
        year: '2019',
      },
      {

        title: 'HTML Fundamentals Course - SoloLearn',
        year: '2019',
      }
    ],
  },
];


const keyFigures = [
  {
    label: 'Years of experience',
    start: 0,
    end: 12,
    duration: 5
  },
  {
    label: 'Satisfied clients',
    start: 0,
    end: 15,
    duration: 5
  },
  {
    label: 'Finished projects',
    start: 0,
    end: 20,
    duration: 5
  },
  {
    label: 'Winning awards',
    start: 0,
    end: 10,
    duration: 5
  }
]


const About = () => {


  const [index, setIndex] = useState(0);


  const metaData = {
    title: 'About - Jawad EL ACHHAB',
    description: 'Explore Jawad EL ACHHAB\'s portfolio and learn more about the skills, experiences, and achievements that define the journey of this talented individual in web development and design. Discover a showcase of projects and a glimpse into the creative mind behind the code.',
  };



  return (
    <>
      <Meta {...metaData} />

      <div className="h-full bg-primary/30 py-32 text-center xl:text-left">
        <Circles />

        {/* avatar img */}
        <motion.div
          variants={fadeIn('right', 0.2)}
          initial="hidden"
          animate="show"
          exit="hidden"
          className="hidden xl:flex absolute bottom-0 -left-[370px]"
        >
          <Avatar />
        </motion.div>

        <div className="container mx-auto h-full flex flex-col items-center xl:flex-row gap-x-6">
          <div className="flex-1 flex flex-col justify-center">
            {/* Title */}
            <motion.h2
              variants={fadeIn('right', 0.2)}
              initial="hidden"
              animate="show"
              exit="hidden"
              className="h2">
              Get to know me!
            </motion.h2>

            {/* Body */}
            <motion.p
              variants={fadeIn('right', 0.4)}
              initial="hidden"
              animate="show"
              exit="hidden"
              className="max-w-[500px] mx-auto xl:mx-0 px-2 xl:px-0">
              {'Welcome to my Full Stack Developer portfolio! I\'m Jawad EL ACHHAB, a dedicated professional passionate about creating seamless and innovative digital solutions. With expertise in both front-end and back-end technologies, I craft robust web applications and user-centric experiences.'}
            </motion.p>
            <motion.p
              variants={fadeIn('right', 0.4)}
              initial="hidden"
              animate="show"
              exit="hidden"
              className="max-w-[500px] mx-auto xl:mx-0 mb-6 xl:mb-12 px-2 xl:px-0">
              {'Explore my projects to witness the intersection of creativity and technical proficiency in the dynamic world of Full Stack Development.'}
            </motion.p>

            {/* key figures */}
            <motion.div
              variants={fadeIn('right', 0.6)}
              initial="hidden"
              animate="show"
              exit="hidden"
              className="hidden md:flex md:max-w-xl xl:max-w-none mx-auto xl:mx-0 mb-8"
            >
              <div className="flex flex-1 xl:gap-x-6">

                {keyFigures.map((figure, index) => {
                  return (
                    <div key={index} className={`relative flex-1  ${keyFigures.length - 1 !== index ? 'after:w-[1px] after:h-full after:bg-white/10 after:absolute after:top-0 after:right-0' : ''}`}>
                      <div className="text-2xl xl:text-4xl font-extrabold text-accent mb-2">
                        <CountUp start={figure.start} end={figure.end} duration={figure.duration} />+
                      </div>
                      <div className="text-xs uppercase tracking-[1px] leading-[1.4] max-w-[100px]">{figure.label}</div>
                    </div>
                  )
                })}

              </div>
            </motion.div>
          </div>


          {/* Info */}
          <motion.div
            variants={fadeIn('right', 0.4)}
            initial='hidden'
            animate='show'
            exit='hidden' className="flex flex-col xl:justify-center  w-full xl:max-w-[48%] h-[480px]">

            {/* Tabs header */}
            <div className="flex gap-x-4 xl:gap-x-8 mx-auto xl:mx-0 mb-4">
              {aboutData.map((item, indexItem) => {
                return (
                  <div key={indexItem}
                    className={`${index === indexItem ? 'text-accent after:w-[100%] after:bg-accent after:transition-all after:duration-300' : 'after:bg-white'} cursor-pointer  capitalize xl:text-lg  relative after:w-8 after:h-[2px]  after:absolute after:-bottom-1 after:left-0`}
                    onClick={() => setIndex(indexItem)}>
                    {item.title}
                  </div>
                )
              })}
            </div>

            {/* Tabs body */}
            <div className="py-2 xl:py-6 flex flex-col gap-y-2 xl:gap-y-4 items-center xl:items-start">
              {aboutData[index].info.map((item, itemIndex) => {
                return (
                  <div key={itemIndex}
                    className="flex-1 flex flex-col md:flex-row max-w-max gap-x-2 items-center text-white/60"
                  >
                    {/* Title */}
                    <div className="font-light mb-2 md:mb-0">{item.title}</div>
                    <div className="hidden md:flex">-</div>
                    <div>{item.year}</div>
                    <div className="flex gap-x-4">
                      {/* Icons */}
                      {item.icons?.map((icon, itemIndex) => {
                        return (
                          <div
                            key={itemIndex}
                            className="text-2xl text-white"
                          >
                            {icon}</div>
                        )
                      })}
                    </div>

                  </div>
                )
              })}
            </div>
          </motion.div>

        </div>
      </div>
    </>
  )

};

export default About;
