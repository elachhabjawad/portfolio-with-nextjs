
import { motion } from 'framer-motion';
import { fadeIn } from '../../variants';
import ServiceSlider from '../../components/ServiceSlider';
import Bulb from '../../components/Bulb';
import Circles from '../../components/Circles';
import Meta from '../../components/Meta';


const Services = () => {


  const metaData = {
    title: 'Services - Jawad EL ACHHAB',
    description: 'Explore a range of tailored web development services by Jawad EL ACHHAB. From intuitive websites to robust web applications, my expertise spans front-end and back-end technologies. Collaborate for innovative solutions that elevate your digital presence.',
  };


  return (
   <>

      <Meta {...metaData} />
      <div className="h-full bg-primary/30 py-36 flex items-center">
        <Circles />
        <div className="container mx-auto">
          <div className="flex flex-col xl:flex-row gap-x-8">
            <section className="text-center flex xl:w-[30vw] flex-col lg:text-left mb-4 xl:mb-0">
              {/* Title */}
              <motion.h2
                variants={fadeIn('up', 0.3)}
                initial="hidden"
                animate="show"
                exit="hidden"
                className="h2 xl:mt-8"
              > My services <span className="text-accent">.</span>
              </motion.h2>
              {/* Body */}
              <motion.p
                variants={fadeIn('up', 0.4)}
                initial="hidden"
                animate="show"
                exit="hidden" className="max-w-[400px] mx-auto lg:mx-0"
                >
                {'Discover a range of tailored services designed to elevate your digital presence.'}
              </motion.p>
              <motion.p
                variants={fadeIn('up', 0.4)}
                initial="hidden"
                animate="show"
                exit="hidden" className="mb-4 max-w-[400px] mx-auto lg:mx-0"
                >
                {'From crafting intuitive websites to developing robust web applications, my expertise spans both front-end and back-end technologies. Let\'s collaborate to bring your ideas to life with innovative solutions that resonate with your audience.'}
              </motion.p>

            </section>
            <motion.section
              variants={fadeIn('down', 0.6)}
              initial="hidden"
              animate="show"
              exit="hidden"
              className="w-full xl:max-w-[65%]"
            >
              {/* Slider */}
              <ServiceSlider />
            </motion.section>
          </div>
        </div>
        <Bulb />
      </div>
   </>
  )
  
 ;
};

export default Services;
