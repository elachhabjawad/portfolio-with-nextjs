import { motion } from 'framer-motion';
import { fadeIn } from '../../variants';
import TestimonialSlider from '../../components/TestimonialSlider';
import Meta from '../../components/Meta';


const Testimonials = () => {


  const metaData = {
    title: 'Testimonials - Jawad EL ACHHAB',
    description: 'Read testimonials and reviews about Jawad EL ACHHAB\'s work. Discover what clients and collaborators have to say about the quality, expertise, and professionalism demonstrated in projects. Explore the positive experiences shared by those who have worked with Jawad.',
  };

  return (
    <>
      <Meta {...metaData} />
      <div className="h-full bg-primary/30 py-32 text-center">
        <div className="container mx-auto h-full flex flex-col justify-center">
          {/* Title */}
          <motion.h2
            variants={fadeIn('up', 0.3)}
            initial="hidden"
            animate="show"
            exit="hidden"
            className="h2 mb-8 xl:mb-0"
          >
            What clients <span className="text-accent">say.</span>
          </motion.h2>
          {/* Slider */}
          <motion.section
            variants={fadeIn('up', 0.4)}
            initial="hidden"
            animate="show"
            exit="hidden"
          >
            <TestimonialSlider />
          </motion.section>
        </div>
      </div></>
  )


};

export default Testimonials;



